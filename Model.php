<?php 

class Model {

	private $pdo;
	private $table = 'users';
	private $primaryKey = 'id';

	public function __construct() {
		try {
			$pdo = new PDO('mysql:host=localhost;dbname=ajax_project;charset=utf8', 'root', '');
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->pdo = $pdo;
		}
		catch (PDOException $e) {
			echo $e->getMessage();
			die();
		}
	}

	public function query($sql, $parameters = []) {
		$query = $this->pdo->prepare($sql);
		$query->execute($parameters);
		return $query;
	}

	public function total() {
		$query = $this->query("SELECT COUNT(*) FROM {this->$table}");
		$row = $query->fetch();
		return $row[0];
	}

	public function findById($value) {

		$query = "SELECT * FROM {$this->table} WHERE {$this->primaryKey} = :value";
		$parameters = [
			'value' => $value
		];
		$query = $this->query($query, $parameters);
		while ($row = $query->fetch(PDO::FETCH_OBJ)) {
			$users[] = $row;
		}
		return $users;

	}

	public function insert($fields) {

		$query = "INSERT INTO {$this->table} (";
		foreach ($fields as $key => $value) {
			$query .= '`' . $key . '`,';
		}
		$query = rtrim($query, ',');
		$query .= ") VALUES (";
		foreach($fields as $key => $value) {
			$query .= ":" . $key . ",";
		}
		$query = rtrim($query, ",");
		$query .= ")";
		$this->query($query, $fields);

	}

	public function update($fields) {
		$query = ' UPDATE `' . $this->table .'` SET ';

		foreach ($fields as $key => $value) {
			$query .= '`' . $key . '` = :' . $key . ',';
		}

		$query = rtrim($query, ',');

		$query .= ' WHERE `' . $this->primaryKey . '` = :primaryKey';

		//Set the :primaryKey variable
		$fields['primaryKey'] = $fields['id'];

		// $fields = $this->processDates($fields);
		echo $query;

		$this->query($query, $fields);
	}


}

$model = new Model;
echo '<pre>', var_dump($model->findById(3)), '</pre>';
$users = $model->findById(3);
foreach ($users as $user) {
	echo $user->name;
}

// $model->insert(['name' => 'Hugh Mungus', 'age' => 28]);


$model->update(['id' => 2, 'age' => 21]);

