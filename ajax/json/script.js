(function() {
	
	var getUser = document.getElementById('js-getUser')
	var getUsers = document.getElementById('js-getUsers')
	var userDetails = document.getElementById('js-userDetails')
	var usersDetails = document.getElementById('js-usersDetails')

	getUser.addEventListener('click', loadUser)
	getUsers.addEventListener('click', loadUsers)

	function loadUser() {
		var xhr = new XMLHttpRequest()

		xhr.open('GET', 'user.json', true)

		xhr.onload = function() {
			if (this.status == 200) {
				// console.log(this.responseText)
				var user = JSON.parse(this.responseText)
				// console.log(user.name)
				var output = 
				`
				<ul>
					<li> ${user.id} </li>
					<li> ${user.name} </li>
					<li> ${user.email} </li>
				</ul>
				`

				userDetails.innerHTML = output

			}
		}

		xhr.send()
	}

	function loadUsers() {
		var xhr = new XMLHttpRequest()

		xhr.open('GET', 'users.json', true)

		xhr.onload = function() {
			if (this.status == 200) {
				// console.log(this.responseText)
				var users = JSON.parse(this.responseText)
				// console.log(user.name)
				
				var output = ''
				
				for (var i in users) {

					output += 
					`
					<ul>
						<li> ${users[i].id} </li>
						<li> ${users[i].name} </li>
						<li> ${users[i].email} </li>
					</ul>
					`

				}

				usersDetails.innerHTML = output

			}
		}

		xhr.send()
	}

}())