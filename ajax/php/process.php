<?php 

try {
	$pdo = new PDO('mysql:host=localhost;dbname=ajax_project', 'root', '');
	echo 'Connected';
} catch (PDOException $e) {
	echo "Error: {$e->getMessage()}";
	die();
}



if (isset($_GET['name'])) {
	echo "GET: Your name is {$_GET['name']}";
}

if (isset($_POST['name'])) {
	// echo "POST: Your name is {$_POST['name']}";
	$name = htmlentities($_POST['name'], ENT_QUOTES);

	$query = $pdo->prepare("INSERT INTO users (name) VALUES (:name)");
	$query->bindParam(':name', $name);

	if ($query->execute()) {
		echo "New user added {$name}";
	}
	else {
		echo "Error: Can't add user";
	}
}