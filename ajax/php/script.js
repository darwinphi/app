(function() {

	var btn = document.getElementById('js-button')
	var getForm = document.getElementById('js-getForm')
	var postForm = document.getElementById('js-postForm')

	btn.addEventListener('click', getName)
	getForm.addEventListener('submit', getName)
	postForm.addEventListener('submit', postName)

	function getName(event) {
		event.preventDefault()
		var name1 = document.getElementById('js-name1').value

		var xhr = new XMLHttpRequest

		xhr.open('GET', `process.php?name=${name1}`, true)

		xhr.onload = function() {
			console.log(this.responseText)
		}

		xhr.send()
	}

	function postName(event) {
		event.preventDefault()

		var name2 = document.getElementById('js-name2').value
		var params = `name=${name2}`

		var xhr = new XMLHttpRequest

		xhr.open('POST', `process.php`, true)
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')

		xhr.onload = function() {
			console.log(this.responseText)
		}

		xhr.send(params)
	}

}())