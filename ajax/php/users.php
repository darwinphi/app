<?php 

try {
	$pdo = new PDO('mysql:host=localhost;dbname=ajax_project', 'root', '');
	// echo 'Connected';
} catch (PDOException $e) {
	echo "Error: {$e->getMessage()}";
	die();
}

$query = $pdo->prepare("SELECT * FROM users");

$query->execute();
$rows = $query->fetchAll(PDO::FETCH_ASSOC);


echo json_encode($rows);
