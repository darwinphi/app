(function() {
	
	var btn = document.getElementById('js-btn')

	// Create an event listener
	btn.addEventListener('click', loadText)

	// loadText Function
	function loadText() {
		// Create the xhr object
		var xhr = new XMLHttpRequest()

		// Open - type, url/file, if async or not
		xhr.open('GET', 'sample.txt', true)

		// OPTIONAL - used for laoders
		// xhr.onprogress = function() {
		// 	console.log(`READYSTATE: ${xhr.readyState}`)
		// }

		// xhr.onload = function() {
		// 	// Can also be xhr.status
		// 	if (this.status == 200) {
		// 		console.log(this.responseText)
		// 	}
		// }
		
		xhr.onreadystatechange = function() {
			console.log(`READYSTATE: ${xhr.readyState}`)
			var text = document.getElementById('js-text')
			
			if (this.readyState == 4 && this.status == 200) {
				// console.log(this.responseText)
				
				text.textContent = this.responseText
			}
			else if (this.status == 404) {
				text.textContent = "Not found"
			}
		}

		xhr.onerror = function() {
			console.log('Request Error')
		}

		// Sends request
		xhr.send()
	}

	// -- HTTP Status --
	// 200 - Ok
	// 403 - Forbidden
	// 404 - Not found
	
	// -- readyState values --
	// 0 - request not initiated
	// 1 - server connection established
	// 2 - request received
	// 3 - processiong request
	// 4 - request is finised and response is ready
	

}())