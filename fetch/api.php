<?php 

try {
	$pdo = new PDO("mysql:host=localhost;dbname=ajax_project", "root", "");
	// echo "Connected <br>";
} catch (PDOException $e) {
	echo $e->getMessage();
}

$result = $pdo->query("SELECT * FROM users");
while ($row = $result->fetch(PDO::FETCH_OBJ)) {
	$users[] = $row;
}

if ($_GET['key'] !== '123') {
	echo "You don't have permission to access this url";
}
else {
	echo json_encode($users);
}

