var url = 'api.php?key=123';
var names = [];
fetch(url)
	.then(blob => blob.json())
	.then(data => {
		var lists =  document.getElementById('js-lists')
		names.push(...data)
		var users = names.map(user => `<li> ${user.name} </li>`).join("")
		lists.innerHTML = users
	})
	.catch(error => console.log('Cannot fetch data', error))


// var list1 = document.getElementById('js-lists');
// var list2 = document.querySelector('#js-lists');
// var list3 = document.querySelectorAll('#js-lists');
// console.log(list1)

var d = 2;
function calculate() {
	var d = 1;
	console.log(this.d)
}

calculate()

var shape = {
	d: 5,
	calculate() {
		console.log(this.d + 1)
	}
}

shape.calculate()