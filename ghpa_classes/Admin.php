<?php 

class Admin extends \PDO{

	private $_db;

	public function __construct(Database $db) {
		$this->_db = $db;
	}

	public function getAnnouncement() {
		$sql = "SELECT * FROM announcement ORDER BY date_posted DESC LIMIT 1";
		$values = [];

		$this->_db->query($sql, $values);

		if ($this->_db->count()) { return $this->_db->onlyResult(); }
		else { return false; }
		
	}

	public function getEmployer($id) {
		$sql = "SELECT * FROM employers WHERE user_id = ?";
		$values = [$id];

		$this->_db->query($sql, $values);
		return $this->_db->onlyResult();
	}

	public function getAllEmployers() {
		$sql = "SELECT * FROM employers";
		$values = [];

		$this->_db->query($sql, $values);
		return $this->_db->getResults();		
	}

	public function createEmployer($user_values = [], $employer_values = []) {
		$sql = "INSERT INTO users (user_id, user_type, email, password, date_joined) 
				VALUES (?, ?, ?, ?, NOW())";
		if ($this->_db->query($sql, $user_values)) {
			$sql2 = "INSERT INTO employers
					(user_id, email, company_name, employer_name, company_overview, contact, country, address) 
				VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
			if ($this->_db->query($sql2, $employer_values)) {
				return true;
			}
			else {
				return false;
			}
		}
	}

	public function editEmployer($user_values = [], $employer_values = []) {
		$sql = "UPDATE users SET email = ? WHERE user_id = ?";
		if ($this->_db->query($sql, $user_values)) {
			$sql2 = "UPDATE employers
					SET email = ?, company_name = ?, employer_name = ?, company_overview = ?, contact = ?, website = ?, country = ?, address = ? WHERE user_id = ?";
			$this->_db->query($sql2, $employer_values);
		}
	}



	public function createJob($values = []) {
		$sql = "INSERT INTO job_post (id, title, employer_id, job_id, attain_id, salary, min_exp, job_details, date_posted)
			VALUES (?, ?, ?, ?, ?, ?, ?, ?, NOW())";
		$this->_db->query($sql, $values);
	}

	public function getTable($table) {
		$sql = "SELECT * FROM {$table}";
		$values = [];

		$this->_db->query($sql, $values);
		return $this->_db->getResults();
	}

	public function getJobs($start, $perPage) {
		$sql = "SELECT SQL_CALC_FOUND_ROWS *, job_post.id AS job_post_id, job_post.title AS job_post_title FROM job_post 
			INNER JOIN employers ON
			job_post.employer_id = employers.user_id
			INNER JOIN jobs ON
			job_post.job_id = jobs.id
			INNER JOIN attain ON
			job_post.attain_id = attain.id
			ORDER BY date_posted DESC
			LIMIT {$start}, {$perPage}
		";
		$values = [];

		$this->_db->query($sql, $values);
		return $this->_db->getResults();
	}

	public function getUserSkills() {
		if (Session::exists('user_id')) {
			$id = Session::get('user_id');
			$sql = "SELECT * FROM users WHERE user_id = ?";
			$values = [$id];

			$this->_db->query($sql, $values);
			return $this->_db->onlyResult()->skill1;
		} else {
			return false;
		}
		
	}

	public function getJobsBySkills($start, $perPage) {

		$skill1 = $this->getUserSkills();

		$sql = "SELECT SQL_CALC_FOUND_ROWS *, job_post.id AS job_post_id, job_post.title AS job_post_title FROM job_post 
			INNER JOIN employers ON
			job_post.employer_id = employers.user_id
			INNER JOIN jobs ON
			job_post.job_id = jobs.id
			INNER JOIN job_division ON 
			jobs.job_division_id = job_division.id
			INNER JOIN attain ON
			job_post.attain_id = attain.id
			WHERE job_division_id = ?
			ORDER BY date_posted DESC
			LIMIT {$start}, {$perPage}
		";
		$values = [$skill1];

		$this->_db->query($sql, $values);
		return $this->_db->getResults();
	}

	public function getJobsBySkillsSelect($skill1) {

		$sql = "SELECT SQL_CALC_FOUND_ROWS *, job_post.id AS job_post_id, job_post.title AS job_post_title FROM job_post 
			INNER JOIN employers ON
			job_post.employer_id = employers.user_id
			INNER JOIN jobs ON
			job_post.job_id = jobs.id
			INNER JOIN job_division ON 
			jobs.job_division_id = job_division.id
			INNER JOIN attain ON
			job_post.attain_id = attain.id
			WHERE job_division_id = ?
			ORDER BY date_posted DESC
			LIMIT 0, 6
		";
		$values = [$skill1];

		$this->_db->query($sql, $values);
		return $this->_db->getResults();
	}

	

	public function getTotalJobs() {
		$sql = "SELECT FOUND_ROWS() as total";
		$values = [];

		$this->_db->query($sql, $values);
		$total = $this->_db->onlyResult()->total;

		return $total;
	}

	public function getSearch($search) {
		$sql = "SELECT SQL_CALC_FOUND_ROWS *, job_post.id AS job_post_id, job_post.title AS job_post_title FROM job_post 
			INNER JOIN employers ON
			job_post.employer_id = employers.user_id
			INNER JOIN jobs ON
			job_post.job_id = jobs.id
			INNER JOIN job_division ON 
			jobs.job_division_id = job_division.id
			INNER JOIN attain ON
			job_post.attain_id = attain.id
			WHERE job_title = ?
			ORDER BY date_posted DESC
			LIMIT 0, 6
		";
		$values = [$search];

		$this->_db->query($sql, $values);
		return $this->_db->getResults();
	}

	public function getPagination($pages, $page) {
		for ($i = 1; $i <= $pages; $i++) { 
			$output =  '<a href="?page=' . $i . '"';
			if ($page === $i) {
				$output .= ' class="selected"';
			}
			$output .= '>';
			$output .= $i;
			$output .= '</a>';

			echo $output;
		}
		
	}

	public function getTracks() {
		$sql = "SELECT * FROM tracks
				INNER JOIN users ON
				tracks.user_id = users.user_id
				ORDER BY login_date DESC
		";
		$values = [];

		$this->_db->query($sql, $values);
		return $this->_db->getResults();
	}

	public function createEvent($title, $date, $desc) {
		$sql = "INSERT INTO events (event_id, title, event_date, description) 
			VALUES (?, ?, ?, ?)";

		$id = Generate::create_guid();
		$values = [$id, $title, $date, $desc];
				
		$this->_db->query($sql, $values);
	}

	public function getAllUsers() {
		$sql = "SELECT *, a.user_id AS a_user_id FROM users a 
				LEFT JOIN employers b
				ON a.user_id = b.user_id
		";
		$values = [];

		$this->_db->query($sql, $values);
		return $this->_db->getResults();
	}

	public function getEditJob($id) {
		$sql = "SELECT * FROM job_post 
			INNER JOIN employers ON
			job_post.employer_id = employers.user_id
			INNER JOIN jobs ON
			job_post.job_id = jobs.id
			WHERE job_post.id = ?
		";
		$values = [$id];
		$this->_db->query($sql, $values);
		return $this->_db->onlyResult();
	}

	public function editJob($id, $title, $job_details) {
		$sql = "UPDATE job_post SET
			title = ?, job_details = ?
			WHERE job_post.id = ?
		";
		$values = [$title, $job_details, $id];
		$this->_db->query($sql, $values);
	}

	public function getSuggestedApplicants($id) {
		$sql = "SELECT * FROM users 
			INNER JOIN job_division ON
			job_division.id = users.skill1
			INNER JOIN jobs ON
			jobs.job_division_id = job_division.id
			INNER JOIN job_post ON
			job_post.job_id = jobs.id
			WHERE job_post.id = ?
		";
		$values = [$id];
		$this->_db->query($sql, $values);
		return $this->_db->getResults();
	}

	public function getJob($id) {
		$sql = "SELECT *, job_post.id AS job_post_id FROM job_post 
				INNER JOIN jobs ON
				job_post.job_id = jobs.id
				WHERE job_post.id = ?
		";
		$values = [$id];
		$this->_db->query($sql, $values);
		return $this->_db->onlyResult();
	}

}