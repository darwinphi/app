<?php 

class Auditrail extends \PDO{

	private $_db, $_id, $_user, $_total;

	public function __construct(Database $db) {
		$this->_db 		= $db;
		$this->_id		= Generate::create_guid();
		$this->_user	= Session::get('user_id');
	}

	private function userDetails() {
		$sql = "SELECT * FROM users WHERE user_id = ?";
		$values = [$this->_user];

		$this->_db->query($sql, $values);
		return $this->_db->onlyResult();
	}

	public function action($string) {
		$firstName = $this->userDetails()->first_name;
		$gender = ($this->userDetails()->gender == 'Male') ? 'his' : 'her';

		switch ($string) {
			case 'login':
				$statement = "{$firstName} logged in";
				break;
			case 'logout':
				$statement = "{$firstName} logged out";
				break;
			case 'update-info':
				$statement = "{$firstName} updated {$gender} info";
				break;
			case 'update-contact':
				$statement = "{$firstName} updated {$gender} contacts";
				break;
			case 'update-education':
				$statement = "{$firstName} updated {$gender} education";
				break;
			case 'update-experience':
				$statement = "{$firstName} updated {$gender} experience";
				break;
			case 'update-password':
				$statement = "{$firstName} changed {$gender} password";
				break;
			case 'upload':
				$statement = "{$firstName} uploaded {$gender} documents";
				break;
			default:
				# code...
				break;
		}

		return $statement;

		// $sql = "INSERT INTO auditrail (audi_id, audi_user, audi_action, audi_date) VALUES (?, ?, ?, NOW())
		// 		";
		// $values = [$this->_id, $this->_user, $statement];
		// $this->_db->query($sql, $values);
	}

	public function getUserAuditrail($id) {
		$sql = "SELECT * FROM auditrail WHERE audi_user = ? ORDER BY audi_date DESC";
		$values = [$id];

		$this->_db->query($sql, $values);
		return $this->_db->getResults();
	}

	public function getUserAudit($id) {
		$sql = "SELECT * FROM audit_trail WHERE user_id = ? AND user_action = ? ORDER BY audit_date DESC";
		$values = [$id, 'UPDATE'];

		$this->_db->query($sql, $values);
		return $this->_db->getResults();
	}

	public function getUserLog($id) {
		$sql = "SELECT * FROM audit_trail WHERE user_id = ? AND user_info = ? ORDER BY audit_date DESC";
		$values = [$id, 'Log'];

		$this->_db->query($sql, $values);
		if ($this->_db->count()) {
			$this->_total = $this->_db->count();
			return $this->_db->getResults();
		} else {
			return false;
		}
		
	}

	public function total() {
		return $this->_total;
	}

	public function createLogin($id, $ip, $location) {
		$sql = "INSERT INTO audit_trail (user_id, user_info, user_action, old_data, new_data, audit_date, ip, location) VALUES (?, ?, ?, ?, ?, NOW(), ?, ?)";
		$values = [$id, 'Log', 'Logged In', 'NONE', 'NONE', $ip, $location];

		$this->_db->query($sql, $values);
	}

	public function createLogout($id, $ip, $location) {
		$sql = "INSERT INTO audit_trail (user_id, user_info, user_action, old_data, new_data, audit_date, ip, location) VALUES (?, ?, ?, ?, ?, NOW(), ?, ?)";
		$values = [$id, 'Log', 'Logged Out', 'NONE', 'NONE', $ip, $location];

		$this->_db->query($sql, $values);
	}
}