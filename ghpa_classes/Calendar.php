<?php 

class Calendar {

	private $month,
			$year,
			$days_of_week,
			$num_days,
			$date_info,
			$day_of_week,
			$_db;

	public function __construct(Database $db, $month, $year, $days_of_week = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat')) {
		$this->_db = $db;
		$this->month = $month;
		$this->year = $year;
		$this->days_of_week = $days_of_week;
		$this->num_days = cal_days_in_month(CAL_GREGORIAN, $this->month, $this->year);
		$this->date_info = getDate(strtotime('first day of', mktime(0,0,0, $this->month, 1, $this->year)));
		$this->day_of_week = $this->date_info['wday'];
	}

	public function show() {
		// Month and year caption
		$output = '<table class="calendar">';
		$output .= '<caption>' . $this->date_info['month'] . ' ' . $this->year . '</caption>';
		$output .= '<tr>'; 

		// Days of the week header
		foreach ($this->days_of_week as $day) {
			$output .= '<th class="day-title">' . $day .'</th>';
		}

		// Close header row and open first row of days
		$output .= '</tr><tr>';

		// If first day of a month does not fill on a sunday, then we need to fill
		// beginning space using colspan
		if ($this->day_of_week > 0) {
			$output .= '<td colspan="' . $this->day_of_week . '"></td>';
		}

		//  Start num_days counter
		$current_day = 1;

		while ($current_day <= $this->num_days) {
			// Reset 'day of week' counter and close each row if end of row
			if ($this->day_of_week == 7) {
				$this->day_of_week = 0;
				$output .= '</tr><tr>';
			}
			
			$title = $this->getDate($this->year, $this->month, $current_day);

			// Build each day cell
			$output .= '<td class="day">' . $current_day . '<br><br>';
			$output .= $title;
			$output .= '</td>';

			// Increment counters
			$current_day++;
			$this->day_of_week++;
		}


		// Once num_days counter stops, if day of week counter is not 7, then
		// we need to fill the remaining space on the row using colspan
		if ($this->day_of_week != 7) {
			$remaining_days = 7 - $this->day_of_week;
			$output .= '<td colspan="' . $remaining_days . '"></td>';
		}

		// Close final row and table
		$output .= '</tr>';
		$output .= '</table>';

		// Output
		echo $output;

	}

	
	public function getDate($year, $month, $day) {
		$date = $year . '-' . $month . '-' . $day;
		$sql = "SELECT * FROM events WHERE event_date = ?";
		$values = [$date];
		$this->_db->query($sql, $values);
	

		if ($this->_db->count()) {
			$title = $this->_db->onlyResult()->title;
		} else {
			$title = '';
		}

		return $title;
	}
}

?>


