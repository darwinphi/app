<?php 

class Database extends \PDO {

	private $_host = 'localhost',
			$_dbname = 'globalh1_v3',
			$_user = 'globalh1_v3',
			$_password = '^d&)Xca5-mz^';

	private $_pdo,
			$_results,
			$_query,
			$_columns,
			$_resultsArray,
			$_error = false,
			$_count = 0;

	public function __construct() {
		try {
			$this->_pdo = new PDO(
				"mysql:host={$this->_host};dbname={$this->_dbname}",
				"{$this->_user}",
				"{$this->_password}"
			);
			$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
			echo $e->getMessage();
			die();
		}
	}

	public function query($sql, $values = []) {
		$this->_error = false;
		if ($this->_query = $this->_pdo->prepare($sql)) {
			$position = 1;
			if (count($values)) {
				foreach ($values as $value) {
					$this->_query->bindValue($position, $value);
					$position++;
				}
			}

			$sql = explode(' ', $sql);

			if (in_array("SELECT", $sql)) {
				if ($this->_query->execute()) {
					$this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
					$this->_count = $this->_query->rowCount();	
				} 	else {
					$this->_error = true;	
				}
			} else {
				$this->_query->execute();
				$this->_count = $this->_query->rowCount();
			}	
		}

		return $this;
	}

	public function getResults() {
		return $this->_results;
	}

	public function onlyResult() {
		return $this->getResults()[0];
	}

	public function error() {
		return $this->_error;
	}

	public function count() {
		return $this->_count;
	}

	public function isQuerySuccess() {
		if ($this->_error === true) {
			return false;
		}
		else {
			return true;
		}
	}
}