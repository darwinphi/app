<?php 

class Employer extends \PDO{

	private $_db,
			$_id;

	public function __construct(Database $db) {
		$this->_db = $db;
		$this->_id = Session::get('user_id');
	}

	public function getEmployerDetails() {
		$sql = "SELECT * FROM employers WHERE user_id = ?";
		$values = [$this->_id];

		$this->_db->query($sql, $values);
		return $this->_db->onlyResult();
	}

	public function pendingApplicants() {
		$status = 3;
		$sql = "SELECT * FROM applicant_list  
				INNER JOIN job_post ON 
				applicant_list.post_id = job_post.id
				WHERE employer_id = ? AND approve_employer = ?
		";

		$values = [$this->_id, $status];
		$this->_db->query($sql, $values);

		$num = $this->_db->count();

		
		echo "You have {$num} pending applicants";
	}

	public function getEmployerJobs() {
		$sql = "SELECT *, job_post.id AS job_post_id FROM job_post 
				INNER JOIN jobs ON
				job_post.job_id = jobs.id
				WHERE employer_id = ?
		";
		$values = [$this->_id];
		$this->_db->query($sql, $values);

		return $this->_db->getResults();
	}

	public function getApplicants($job_post_id) {
		$order  = 'first_name';
		$sort	= 'ASC';

		$sql = "SELECT *, status1.status_title AS statusEmployer, status2.status_title AS statusAdmin, job_post.id AS job_post_id FROM applicant_list
			INNER JOIN job_post ON
			applicant_list.post_id = job_post.id
			INNER JOIN users ON
			applicant_list.user_id = users.user_id
			INNER JOIN jobs ON
			job_post.job_id = jobs.id
			INNER JOIN status as status1 ON
			applicant_list.approve_employer = status1.status_id
			INNER JOIN status as status2 ON
			applicant_list.approve_admin = status2.status_id
			WHERE job_post.id = '{$job_post_id}' AND applicant_list.revert = ?
			ORDER BY {$order} {$sort}
		";
		$values = [0];

		$this->_db->query($sql, $values);
		
		
		if ($this->_db->count()) {
			return array($this->_db->getResults(), $this->_db->onlyResult()->job_title, $this->_db->onlyResult()->job_details);
		} else {
			return false;
		}
	}

	public function getApplicant($id) {
		$sql = "SELECT *, status1.status_title AS statusEmployer, status2.status_title AS statusAdmin, job_post.id AS job_post_id FROM applicant_list
			INNER JOIN job_post ON
			applicant_list.post_id = job_post.id
			INNER JOIN users ON
			applicant_list.user_id = users.user_id
			INNER JOIN jobs ON
			job_post.job_id = jobs.id
			INNER JOIN status as status1 ON
			applicant_list.approve_employer = status1.status_id
			INNER JOIN status as status2 ON
			applicant_list.approve_admin = status2.status_id
			WHERE applicant_list.user_id = ?
		";
		$values = [$id];

		$this->_db->query($sql, $values);
		return $this->_db->onlyResult();
	}

	public function sendPassword($company_name, $email, $password) {
		$to = $email;

		$subject = 'Employer Password - Global Hospitality Placement Agency';

		$headers 	 = "From: ghpa@globalhospitality.com.ph" . "\r\n";
		$headers 	.= "MIME-Version: 1.0\r\n";
		$headers 	.= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		$message 	 = "<p>Hi, {$company_name}. You may now login using this email:</p>";
		$message 	.= "<p>{$email}</p>";
		$message 	.= "<p>And this password: {$password}</p>";
		

		$email = mail($to, $subject, $message, $headers);
		if ($email) { return true;	}
		else { return false; }
	}
	

}