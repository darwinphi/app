<?php

class Generate {

	public static function password_hash($var) {
		$password = password_hash($var, PASSWORD_DEFAULT, ['cost' => 10]);
		return $password;
	}

	public static function create_uuid() {
		$data = openssl_random_pseudo_bytes(16);
		assert(strlen($data) == 16);

    	$data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    	$data[8] = chr(ord($data[8]) & 0x3f | 0x80);

    	return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
	}

	public static function create_guid() {
	    if (function_exists('com_create_guid')) {
	        return com_create_guid();
	    }
	    else {
	        mt_srand((double)microtime()*10000); //optional for php 4.2.0 and up.
	        $charid = strtoupper(md5(uniqid(rand(), true)));
	        $hyphen = chr(45);// "-"
	        $uuid = //chr(123) // "{"
	            substr($charid, 0, 8).$hyphen
	            .substr($charid, 8, 4).$hyphen
	            .substr($charid,12, 4).$hyphen
	            .substr($charid,16, 4).$hyphen
	            .substr($charid,20,12);
	            //.chr(125);// "}"
	        return $uuid;
	    }
	}

	public static function guidv4() {
		if (function_exists('com_create_guid') === true)
        return trim(com_create_guid(), '{}');

	    $data = openssl_random_pseudo_bytes(16);
	    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
	    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
	    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
	}

	public static function unique() {
		return md5(uniqid('', true));
	}

	public static function greet() {
		echo 'Hello';
	}
}