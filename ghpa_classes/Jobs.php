<?php 

class Jobs extends \PDO {

	private $_db,
			$_search,
			$_onSearch = false,
			$_id;

	public function __construct(Database $db) {
		$this->_db = $db;
	}

	public function search() {
		if (Input::get('search')) {
			$this->_search = Input::get('search');
			$this->_onSearch = true;
		} else {
			$this->_onSearch = false;
		}

		return $this;
	}

	public function getSearch() {
		$sql = "SELECT * FROM jobs 
				INNER JOIN job_post ON 
				jobs.id = job_post.job_id
				WHERE job_title = ?
		";
		$values = [$this->_search];

		$this->_db->query($sql, $values);
		return $this->_db->getResults();
	}

	public function getJobs() {
		$sql = "SELECT * FROM job_post";
		$values = [];

		$this->_db->query($sql, $values);
		return $this->_db->getResults();
	}

	public function haveSkills() {
		if (User::isLoggedIn()) {
			$id = Session::get('user_id');
			$sql = "SELECT * FROM users WHERE user_id = ?";
			$values = [$id];

			$this->_db->query($sql, $values);
			return $this->_db->onlyResult()->skill1;
		} else {
			return $this->getJobs();
		}
	}



	public function onSearch() {
		return $this->_onSearch;
	}
	
}