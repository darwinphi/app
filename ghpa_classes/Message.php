<?php 

class Message extends \PDO {

	private $_db;

	public function __construct(Database $db) {
		$this->_db = $db;
	}

	public function sendInquiry($name, $company, $email, $phone, $txt) {
		
		$to = 'ghpa@globalhospitality.com.ph';

		$subject = 'Inquiry - Global Hospitality Placement Agency';

		$headers 	 = "From: {$email}" . "\r\n";
		$headers 	.= "MIME-Version: 1.0\r\n";
		$headers 	.= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		$message 	 = "<p>Name: {$name}</p>";
		$message 	.= "<p>Company: {$company}</p>";
		$message 	.= "<p>Phone: {$phone}</p>";
		$message 	.= "<p>Message: {$txt}</p>";
		

		$email = mail($to, $subject, $message, $headers);
		if ($email) { return true;	}
		else { return false; }
	
	}
}