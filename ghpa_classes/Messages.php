<?php 

class Messages extends \PDO{ 

	private $_db,
			$_messages,
			$_empty = true,
			$_id,
			$_dates;

	public function __construct(Database $db) {
		$this->_db = $db;
		$this->_userId = Session::get('user_id');
	}

	public function sendMessageTo($profile, $to, $msg) {
		switch ($to) {
			case 'admin':
				$to = 'C73CC1BB-3992-28AB-FF58-8FE051010DA1';
				break;
			case 'user':
				$to = $to;
				break;
			default:
				
				break;
		}
		
		$msg_id = Generate::create_guid();
		$sql = "INSERT INTO messages (msg_id, user_id, msg_from, msg_to, msg, msg_date)
				VALUES (?, ?, ?, ?, ?, NOW());
		";
		$values = [$msg_id, $profile, $this->_userId, $to, $msg];
		if ($this->_db->query($sql, $values)) {
			return true;
		} else {
			return false;
		}
	}

	public function getMessages() {
		$sql = "SELECT * FROM messages WHERE user_id = ? ORDER BY msg_date ASC";
		$values = [$this->_userId];

		$this->_db->query($sql, $values);

		if ($this->_db->count()) {
			$this->_empty 		= false;
			$this->_messages 	= $this->_db->getResults();
			$this->_dates 	= $this->_db->getResults();
		} else {
			$this->_empty = true;
		}

		return $this;
	}

	public function getAdminMessage($profile) {
		$sql = "SELECT * FROM messages 
				WHERE messages.user_id = ? ORDER BY msg_date ASC
		";
		$values = [$profile];

		$this->_db->query($sql, $values);

		if ($this->_db->count()) {
			$this->_empty 		= false;
			$this->_messages 	= $this->_db->getResults();
			
		} else {
			$this->_empty = true;
		}

		return $this;
	
	}

	public function messages() {
		return $this->_messages;
	}
	public function dates() {
		return $this->_dates;
	}

	public function emptyMsg() {
		return $this->_empty;
	}
}