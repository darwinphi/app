<?php 

class Notification extends \PDO{

	private $_db, $_id, $_notified = false, $_viewNotifyApplicant;

	public function __construct(Database $db) {
		$this->_db 	= $db;
	}

	public function applicantJob($applicantId, $jobId) {
		$notifyId = Generate::create_guid();

		$sql = "INSERT INTO notify_applicant_job 
				(notify_id, notify_user_id, notify_job_id, notify_date)
				VALUES (?, ?, ?, NOW())
		";

		$values = [$notifyId, $applicantId, $jobId];
		$this->_db->query($sql, $values);

		if ($this->_db->count()) { return true; }
		else {	return false; }
	}

	public function applicantNotified($applicantId, $jobId) {
		$sql = "SELECT * FROM notify_applicant_job
				WHERE notify_user_id = ? AND notify_job_id = ?
		";

		$values = [$applicantId, $jobId];

		$this->_db->query($sql, $values);
		if ($this->_db->count()) { return true; }
		else {	return false; }
	}

	public function checkNotifyApplicant($id) {
		$sql = "SELECT * FROM notify_applicant_job
				INNER JOIN job_post ON 
				notify_applicant_job.notify_job_id = job_post.id
				INNER JOIN jobs ON
				job_post.job_id = jobs.id
				WHERE notify_user_id = ?
		";
		$values = [$id];
		$this->_db->query($sql, $values);

		if ($this->_db->count()) {
			$this->_viewNotifyApplicant = $this->_db->onlyResult();
			$this->_notified = true;
		} else {
			$this->_notified = false;
		}

		return $this;
		
	}

	public function viewNotifyApplicant() {
		return $this->_viewNotifyApplicant;
	}

	public function addNotif($id) {
		$sql = "INSERT INTO messages_notif (msg_id, msg_notif_date) VALUES (?, NOW())";
		$values = [$id];

		if ($this->_db->query($sql, $values)) {
			return true;
		} else {
			return false;
		}
	}

	public function countNotif($id) {
		$sql = "SELECT * FROM messages_notif WHERE msg_id = ? AND msg_status = ?";
		$values = [$id, 1];

		$this->_db->query($sql, $values);
		if ($this->_db->count()) {
			return $this->_db->count();
		} else {
			return "0";
		}
	}

	public function unreadNotif($id) {
		$sql = "UPDATE messages_notif SET msg_status = 0 WHERE msg_id = ?";
		$values = [$id];

		$this->_db->query($sql, $values);
		if ($this->_db->count()) {
			return true;
		} else {
			return false;
		}
	}

	public function getAllUsers() {
		
		$sql = "SELECT *, a.user_id AS a_user_id FROM users a 
				LEFT JOIN employers b
				ON a.user_id = b.user_id
				ORDER BY a.new_message DESC
		";
		$values = [];

		$this->_db->query($sql, $values);
		return $this->_db->getResults();
	}

	public function notified() {
		return $this->_notified;
	}
}