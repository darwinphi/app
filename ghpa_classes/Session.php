<?php 

class Session {

	
	public static function exists($name) {
		return (isset($_SESSION[$name])) ? true : false;
	}

	public static function put($name, $value) {
		return $_SESSION[$name] = $value;
	}

	public static function get($name) {
		return $_SESSION[$name];
	}

	public static function delete($name) {
		if(self::exists($name)) {
			unset($_SESSION[$name]); 
		}
	}

	public static function flash($name, $string = '') {
		if(self::exists($name)) {
			$session = self::get($name);
			self::delete($name);
			return $session;
		} else {
			self::put($name, $string);
		}
	}

	public static function type($number) {
		$userType = self::get('user_type');
		$type = ($userType == $number) ? true : false;
		return $type;
	}

	public static function access($name) {
		if (!self::exists('user_id')) {
			Redirect::to(404);
		} else {
			switch ($name) {
				case 'applicant':
					if (!self::type(1)) {
						Redirect::to(404);
					}
					break;
				case 'admin':
					if (!self::type(2)) {
						Redirect::to(404);
					}
					break;
				case 'admin&applicant':
					if (!self::type(2) && !self::type(1)) {
						Redirect::to(404);
					}
					break;
				case 'admin&employer':
					if (!self::type(2) && !self::type(3)) {
						Redirect::to(404);
					}
					break;
				case 'all':
					if (!self::type(2) && !self::type(1) && !self::type(3)) {
						Redirect::to(404);
					}
					break;
				case 'employer':
					if (!self::type(3)) {
						Redirect::to(404);
					}
					break;
				default:
					# code...
					break;
			}
			
		}
	}

	public static function expire() {
		$expireAfter = 30;

		if (self::exists('last_action')) {
			$secondsInactive = time() - self::get('last_action');
			$expireAfterSeconds = $expireAfter * 60;

			if ($secondsInactive >= $expireAfterSeconds) {
				
				session_destroy();
				echo "
					<script>
						window.location.href = 'index.php';
					</script>
				";
			}

		}
		self::put('last_action', time());
	}
	
}