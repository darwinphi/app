<?php 

class Token {
	public static function generate() {
		return Session::put('token', md5(uniqid()));
		// $_SESSION['token'] = jnasdf6793;
	}

	public static function check($token) {
		$tokenName = 'token';
		if (Session::exists($tokenName) && $token === Session::get($tokenName)) {
			Session::delete($tokenName);
			return true;
		}
		return false;
	}
}