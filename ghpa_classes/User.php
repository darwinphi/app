<?php 

class User extends \PDO{

	private $_db,
			$_error = '',
			$_isLoggedIn = false,
			$_passed = false,
			$_isActive = false;

	public function __construct(Database $db) {
		$this->_db = $db;
	}

	public function create($values = [], $email, $id) {

		$sql = "INSERT INTO users (user_id, user_type, first_name, middle_name, last_name, gender, age, birthdate, email, password, date_joined) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())";
 		
		if (!$this->_db->query($sql, $values)) {
			throw new Exception('There was a problem creating account');
		} else {
			if ($this->updateActiveCode($id, $email)) {
				$this->autoLogin($email);
			}
		}
	
	}

	public function autoLogin($email) {
		$sql = "SELECT * FROM users WHERE email = ?";
		$values = [$email];
		$this->_db->query($sql, $values);

		$userId 	= $this->_db->onlyResult()->user_id;
		$userType 	= $this->_db->onlyResult()->user_type;
		$firstName 	= $this->_db->onlyResult()->first_name;

		Session::put('user_id', $userId);
		Session::put('user_type', $userType);
		Session::put('first_name', $firstName);

		Redirect::to('applicant-info.php');
	}

	public function login($email, $password) {
		$sql = "SELECT * FROM users WHERE email = ?";
		$values = [$email];
		$this->_db->query($sql, $values);

		if ($this->_db->count()) {
			
			$userId 	= $this->_db->onlyResult()->user_id;
			$userType 	= $this->_db->onlyResult()->user_type;
			$firstName 	= $this->_db->onlyResult()->first_name;

			$truePassword 	= $this->_db->onlyResult()->password;
			$givenPassword 	= $password;

			$sessionId = uniqid(mt_rand(), true);

			if (password_verify($givenPassword, $truePassword)) {
				Session::put('user_id', $userId);
				Session::put('user_type', $userType);
				Session::put('first_name', $firstName);
				Session::put('session_id', $sessionId);

				
				$this->_isLoggedIn = true;
				return true;
				
			} else {				
				$this->_error = 'Either email or password is incorrect';
				return false;
			}
		} else {
			$this->_error = 'Either email or password is incorrect';
		}
	}

	public static function isLoggedIn() {
		$isLoggedIn = (Session::exists('user_id')) ? true : false;
		return $isLoggedIn;
		$this->_isLoggedIn = true;
	}


	public function changePassword($pass) {
		$pass = Generate::password_hash($pass);
		$id = Session::get('user_id');

		$sql = "UPDATE users SET password = ? WHERE user_id = ?";
		$values = [$pass, $id];

		$this->_db->query($sql, $values);

		if ($this->_db->count()) {
			$this->_passed = true;
		}
		return $this;
	}

	public function exists($id) {
		$sql = "SELECT * FROM users WHERE user_id = ?";
		$values = [$id];

		$this->_db->query($sql, $values);

		if ($this->_db->count()) {
			return true;
		} else {
			return false;
		}
	}

	public function isActive() {
		if (Session::exists('user_id')) {
			$sql = "SELECT * FROM users WHERE user_id = ?";
			$values = [Session::get('user_id')];

			$this->_db->query($sql, $values);
			$active = $this->_db->onlyResult()->active;
			$type = $this->_db->onlyResult()->user_type;

			if 		($active == 1 && $type == 1) 	{ return true; }
			elseif 	($active == 0 && $type == 1) 	{ return false; } 
			elseif 	($type == 2) 	{ return true; } 
			elseif 	($type == 3) 	{ return true; } 
			else 	{ return false; }

		} else {
			return false;
		}
	}

	public function updateActiveCode($id, $email) {

		$activeCode = Generate::create_guid();
		$userId 	= $id;

		$sql = "UPDATE users SET active_code = ? WHERE user_id = ?";
		$values= [$activeCode, $userId];
		$this->_db->query($sql, $values);

		if ($this->_db->count()) {
			if ($this->sendEmailVerification($email, $activeCode)) {
				return true;
			}
		} else {
			return false;
		}
	}

	public function sendEmailVerification($email, $id) {
		$to = $email;

		$subject = 'Email Verification - Global Hospitality Placement Agency';

		$headers = "From: ghpa@globalhospitality.com.ph" . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		$message  	= '<img src="http://globalhospitality.com.ph/pics/ghpa_logo.jpeg" alt="global logo" width="100" height="40">';

		$message .= "<p>Please confirm your email by click this link</p>";
		$message .= "<a href='globalhospitality.com.ph/applicant-activate.php?code={$id}'>Confirmation Link </a>";
		$message .= "or copy and paste this link in the url: ";
		$message .= "<p>globalhospitality.com.ph/applicant-activate.php?code={$id}</p>";

		$email = mail($to, $subject, $message, $headers);
		if ($email) { return true;	}
		else { return false; }
	}

	public function activateAccount() {

		if (Input::get('code')) {
			$code =  Input::get('code');
			$sql = "UPDATE users SET active = ? WHERE active_code = ?";
			$values= [1, $code];
			
			if (!$this->_db->query($sql, $values)) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	public function sendEmailForgotPassword($email, $code, $firstName, $lastName) {
		$to = $email;

		$subject = 'Forgot Password - Global Hospitality Placement Agency';

		$headers = "From: ghpa@globalhospitality.com.ph" . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		$message = "<p>Hi {$firstName} {$lastName}</p>";
		$message .= "<p>We have received a password change request for your account</p>";
		$message .= "<p></p>";
		$message .= "If you made this request, then please click the link below: ";
		$message .= "<a href='globalhospitality.com.ph/renew-password.php?code={$code}'>Confirmation Link </a>";
		$message .= "or copy and paste this link in the url: ";
		$message .= "<p>globalhospitality.com.ph/renew-password.php?code={$code}</p>";
		$message .= "<p>If you did not ask to change your password please ignore this email</p>";
		$message .= "<p>Best regards, </p>";
		$message .= "<p>GHPA Team</p>";
		
		$email = mail($to, $subject, $message, $headers);
		if ($email) { return true;	}
		else { return false; }
	}

	public function updateForgotPasswordCode($email) {
		$code = Generate::create_guid();
		$email = $email;

		$sql = "UPDATE users SET forgot_password_code = ? WHERE email = ?";
		$values= [$code, $email];
		$this->_db->query($sql, $values);

		if ($this->_db->count()) {
			$sql = "SELECT * FROM users WHERE email = ?";
			$values= [$email];
			$this->_db->query($sql, $values);
			$userDetails = $this->_db->onlyResult();
			$firstName 	= $userDetails->first_name;
			$lastName 	= $userDetails->last_name;

			if ($this->sendEmailForgotPassword($email, $code, $firstName, $lastName)) {
				return true;
			}
		} else {
			return false;
		}
	}

	public function updatePassword($code, $pass) {
		$pass = Generate::password_hash($pass);

		$sql = "UPDATE users SET password = ? WHERE forgot_password_code = ?";
		$values = [$pass, $code];

		$this->_db->query($sql, $values);

		if ($this->_db->count()) {
			return true;
		}
		return false;
	}



	public static function getIPAddress() {
	    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	      $ip=$_SERVER['HTTP_CLIENT_IP'];
	    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	    } else {
	      $ip=$_SERVER['REMOTE_ADDR'];
	    }
	    return $ip;
	}

	public static function geoLocate($place, $ip = null) {
		if ($ip != null) {
			$ip = $ip;
		} else {
			$ip = self::getIPAddress();
		}
		
		// $ip = "221.192.199.49";
		$loc = @unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
		switch ($place) {
			case 'city':
				return $loc['geoplugin_city'];
				break;
			case 'region':
				return $loc['geoplugin_regionName'];
				break;
			case 'countryCode':
				return $loc['geoplugin_countryCode'];
				break;	
			case 'countryName':
				return $loc['geoplugin_countryName'];
				break;				
			default:
				# code...
				break;
		}
	}

	public function greetUser() {
		if (self::isLoggedIn()) {
			switch (Session::get('user_type')) {
				case 1:
					return 'Hello ' . Session::get('first_name') . '!';
					break;
				case 2:
					return "Hello Admin!";
					break;
				case 3:
					$sql = "SELECT * FROM employers WHERE user_id = ?";
					$values = [Session::get('user_id')];
					$this->_db->query($sql, $values);
					$companyName = $this->_db->onlyResult()->company_name;

					return "Hello {$companyName}!";
					break;
				default:
					# code...
					break;
			}
		}
		else {
			return 'Already Signed-Up? Log In';
		}
		
	}



	public function getStatus() {
		$profile = Input::get('profile');
		$user_id = Session::get('user_id');

		if (!$this->isApplied([$user_id])) {
			return false;
		} else {
			$sql = "SELECT * FROM applicant_list 
			INNER JOIN job_post ON 
			applicant_list.post_id = job_post.id
			INNER JOIN jobs ON 
			job_post.job_id = jobs.id
			INNER JOIN employers ON
			job_post.employer_id = employers.user_id
			WHERE applicant_list.user_id = ?";

			if (Input::get('profile')) { $values = [$profile]; } 
			else { $values = [$user_id]; }

			$this->_db->query($sql, $values);
			
			$offer_letter 		= $this->_db->onlyResult()->offer_letter;
			$passport 			= $this->_db->onlyResult()->passport;
			$nbi 				= $this->_db->onlyResult()->nbi;
			$medical 			= $this->_db->onlyResult()->medical;
			$visa 				= $this->_db->onlyResult()->visa;
			$oec 				= $this->_db->onlyResult()->oec;
			$ticket 			= $this->_db->onlyResult()->ticket;
			$approve_employer 	= $this->_db->onlyResult()->approve_employer;
			$approve_admin 		= $this->_db->onlyResult()->approve_admin;

			$job_title 			= $this->_db->onlyResult()->job_title;
			$company_name 		= $this->_db->onlyResult()->company_name;

			if (empty($offer_letter) && empty($passport) && empty($nbi) && empty($medical) && empty($visa) && empty($oec) && empty($ticket) && empty($approve_employer) && empty($approve_admin) && empty($job_title) && empty($company_name)) {
				return false;
			} else {
				return array($offer_letter, $passport, $nbi, $medical, $visa, $oec, $ticket, $approve_employer, $approve_admin, $job_title, $company_name);
			}

		}
		
	}

	public function uploadPicture($values = []) {
		$sql = "UPDATE users SET picture = ? WHERE user_id = ?";
		$this->_db->query($sql, $values);
	}

	public function uploadDocument($values = []) {
		$sql = "UPDATE users SET document = ? WHERE user_id = ?";
		$this->_db->query($sql, $values);
	}

	public function uploadCertificate($values = []) {
		$sql = "UPDATE users SET certificate = ? WHERE user_id = ?";
		$this->_db->query($sql, $values);
	}

	/**
	 * Method for selecting picture from navigation
	 * @param  string $id The user's id
	 * @return string     The picture's source value
	 */
	public function selectPicture($id) {
		$sql = "SELECT * FROM users WHERE user_id = ?";
		$values = [$id];
		$this->_db->query($sql, $values);

		$picture 	= $this->_db->onlyResult()->picture;
		$gender		= $this->_db->onlyResult()->gender;

		if (!empty($picture)) {
			$picture = $picture;
		} elseif (empty($picture) && $gender == 'Male') {
			$picture = 'pictures/male.png';
		} elseif (empty($picture) && $gender == 'Female') {
			$picture = 'pictures/female.png';
		} elseif (empty($picture)) {
			$picture = 'pictures/avatar.png';
		}

		return $picture;
	}

	public function getDocument($id = []) {
		$sql = "SELECT * FROM users WHERE user_id = ?";
		
		$this->_db->query($sql, $id);
		$document =  $this->_db->onlyResult()->document;
		return $document;
	}

	public function getCertificate($id = []) {
		$sql = "SELECT * FROM users WHERE user_id = ?";
		
		$this->_db->query($sql, $id);
		return  $this->_db->onlyResult()->certificate;
	}

	public function isApplied($id = []) {
		$sql = "SELECT * FROM applicant_list WHERE user_id = ?";

		$this->_db->query($sql, $id);
		if ($this->_db->count()) {
			return true;
		} else {
			return false;
		}
	}

	public function alreadyApplied($postID) {
		$id = Session::get('user_id');

		$sql = "SELECT * FROM applicant_list WHERE user_id = ? AND post_id = ?";
		$values = [$id, $postID];

		$this->_db->query($sql, $id);
		if ($this->_db->count()) {
			return true;
		} else {
			return false;
		}
	}

	public function isApproved() {
		$sql = "SELECT * FROM applicant_list WHERE user_id = ?";
		$values = [Session::get('user_id')];
		$this->_db->query($sql, $values);

		$result = $this->_db->onlyResult()->approve_admin;
		if ($result == 1) {
			return true;
		}
		elseif ($result == 2 || $result == 3) {
			return false;
		}

	}

	public function isProfileComplete() {
		$id = Session::get('user_id');
		$sql = "SELECT * FROM users WHERE user_id = ?";
		$values = [$id];

		$this->_db->query($sql, $values);

		$first_name 	= $this->_db->onlyResult()->first_name;
		// $middle_name 	= $this->_db->onlyResult()->middle_name;
		$last_name 		= $this->_db->onlyResult()->last_name;
		$age 			= $this->_db->onlyResult()->age;
		$gender 		= $this->_db->onlyResult()->gender;
		$birthplace 	= $this->_db->onlyResult()->birthplace;
		$birthdate 		= $this->_db->onlyResult()->birthdate;
		$civil_status 	= $this->_db->onlyResult()->civil_status;
		$height 		= $this->_db->onlyResult()->height;
		$weight 		= $this->_db->onlyResult()->weight;
		$nationality 	= $this->_db->onlyResult()->nationality;
		$religion 		= $this->_db->onlyResult()->religion;
		$attainment 		= $this->_db->onlyResult()->attainment;
		$school 			= $this->_db->onlyResult()->school;
		$field_of_study 	= $this->_db->onlyResult()->field_of_study;
		$major 				= $this->_db->onlyResult()->major;
		$last_graduated 	= $this->_db->onlyResult()->last_graduated;
		$email 		= $this->_db->onlyResult()->email;
		$mobile_num = $this->_db->onlyResult()->mobile_num;
		// $tele_num 	= $this->_db->onlyResult()->tele_num;
		$region 	= $this->_db->onlyResult()->region;
		$city 		= $this->_db->onlyResult()->city;
		$address 	= $this->_db->onlyResult()->address;
		$document 		= $this->_db->onlyResult()->document;
		$picture 	= $this->_db->onlyResult()->picture;
		$certificate 	= $this->_db->onlyResult()->certificate;

		// $results = [$first_name, $last_name, $middle_name];
		// $results = [$first_name, $last_name, $middle_name, $age, $gender, $birthplace, $birthdate, $civil_status, $height, $weight, $nationality, $religion, $attainment, $school, $field_of_study, $major, $last_graduated, $email, $mobile_num, $tele_num, $region, $city, $address, $document, $picture, $certificate];

		// $value = array_filter($results);

		
		if (empty($first_name) OR
			empty($last_name) OR
			empty($age) OR
			empty($gender) OR
			empty($birthdate) OR
			empty($birthplace) OR
			empty($civil_status) OR
			empty($height) OR
			empty($weight) OR
			empty($nationality) OR
			empty($religion) OR
			empty($attainment) OR
			empty($school) OR
			empty($field_of_study) OR
			empty($major) OR
			empty($gender) OR
			empty($last_graduated) OR
			empty($email) OR
			empty($mobile_num) OR
			empty($city) OR
			empty($address) OR
			empty($document) OR
			empty($picture) OR
			empty($certificate)
		) {
			return false;
		}
		else {
			return true;
		}
	}

	/**
	 * FOR APPLICANT'S STATUS
	 */

	public function getRequirements($id) {
		$sql = "SELECT * FROM users WHERE user_id = ?";
		$values = [$id];
		$this->_db->query($sql, $values);

		$offer_letter		= $this->_db->onlyResult()->offer_letter;
		$passport 			= $this->_db->onlyResult()->passport;
		$nbi 					= $this->_db->onlyResult()->nbi;
		$medical				= $this->_db->onlyResult()->medical_exam;
		$visa 				= $this->_db->onlyResult()->visa;
		$oec					= $this->_db->onlyResult()->oec;
		$ticket				= $this->_db->onlyResult()->ticket;

		return array($offer_letter, $passport, $nbi, $medical, $visa, $oec, $ticket);
	}

	public function deployed($id) {
		$sql = "SELECT * FROM users WHERE user_id = ?";
		$values = [$id];
		$this->_db->query($sql, $values);

		return $this->_db->onlyResult()->currently_deployed;
	}


	public function applying($id) {
		$sql = "SELECT * FROM users WHERE user_id = ?";
		$values = [$id];
		$this->_db->query($sql, $values);

		$currently_applying = $this->_db->onlyResult()->currently_applying;
		if (!empty($currently_applying)) {
			return true;
		} else {
			return false;
		}
	}

	public function applicantDetails($id) {
		$sql = "SELECT * FROM applicant_list 
				INNER JOIN job_post ON
				applicant_list.post_id = job_post.id
				INNER JOIN jobs ON
				job_post.job_id = jobs.id 
				INNER JOIN employers ON
				employers.user_id = job_post.employer_id
				INNER JOIN users ON
				applicant_list.applicant_id = users.currently_applying;
				WHERE applicant_list.user_id = ?
		";
		$values = [$id];
		$this->_db->query($sql, $values);

		if ($this->_db->count()) {
			return $this->_db->onlyResult();
		} else {
			return false;
		}
	}


	/**
	 * END OF APPLICANT STATUS
	 */



	public function isDeployed() {
		$user_id = Session::get('user_id');

		if (!$this->isApplied([$user_id])) {
			return false;
		} else {
			$id = Session::get('user_id');
			$sql = "SELECT * FROM applicant_list WHERE user_id = ?";
			$values = [$id];

			$this->_db->query($sql, $values);

			$result = $this->_db->onlyResult()->is_deployed;
			if ($result == 1) {
				return true;
			}
			elseif ($result == 2) {
				return false;
			}
		}

	}

	public function suggestJobs() {
		$sql = "SELECT * FROM jobs";
		$values = [];

		$this->_db->query($sql, $values);
		return $this->_db->getResults();
	}


	public static function latestJobsTitle() {

		if (Session::exists('user_type')) {
			if (Session::type(1)) {
				echo 'Suggested Jobs';
			} else {
				echo 'Latest Jobs';
			}
		} else {
			echo 'Latest Jobs';
		}
	}

	public function history() {
		if (Input::get('profile')) {
			$id = Input::get('profile');
		} else {
			$id = Session::get('user_id');
		}

		$sql = "SELECT * FROM applicant_list 
				INNER JOIN job_post ON
				job_post.id = applicant_list.post_id
				INNER JOIN jobs ON
				jobs.id = job_post.job_id
				INNER JOIN employers ON
				employers.user_id = job_post.employer_id
				WHERE applicant_list.user_id = ?
			";
		$values = [$id];
		$this->_db->query($sql, $values);
		return $this->_db->getResults();
	}
	
	public function passed() {
		return $this->_passed;
	}

	public function error() {
		return $this->_error;
	}

	public function greet() {
		return 'Hello';
	}
}