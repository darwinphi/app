<?php 

class Validation {
	
	private $_passed = false,
			$_errors = array(),
			$_label,
			$_db = null;

	public function __construct(Database $db) {
		$this->_db = $db;
	}

	public function check($source, $items = array()) {
		foreach ($items as $item => $rules) {
			foreach ($rules as $rule => $rule_value) {
				// echo "{$item} {$rule} must be {$rule_value}";
				// echo "<BR>"; 

				$value = trim($source[$item]);
				$item = escape($item);
				
				
				switch ($rule) {
					case 'label': 
						$this->_label = $rule_value;
						break;	
					case 'required':
						if (empty($value)) {
							$this->addError("{$this->_label} is required");
						}
						break;
					case 'min':
						if(strlen($value) < $rule_value) {
							$this->addError("{$this->_label} must be minimum of {$rule_value} characters");
						}
						break;
					case 'minAge':
						$from = new DateTime($value);
						$to   = new DateTime('today');
						$age = $from->diff($to)->y;

						if ($age < 18){
							$this->addError("{$this->_label} must be 18 years and above");								
						}
						break;
					case 'max':
						if(strlen($value) > $rule_value) {
							$this->addError("{$this->_label} must be maximum of {$rule_value} characters");
						}
						break;
					case 'matches':
						if($value != $source[$rule_value]) {
							$this->addError("Must match {$this->_label}");
						}
						break;
					case 'unique':
						$check = $this->_db->query("SELECT * FROM {$rule_value} WHERE {$item} = ?", array($value));

							if($check->count()) {
								$this->addError("$item already exists");
							}
						break;
					case 'exists':
						$check = $this->_db->query("SELECT * FROM {$rule_value} WHERE {$item} = ?", array($value));

							if (!$check->count()) {
								$this->addError("{$value} is not a registered account. ");
							}
						break;
					case 'email': 
						if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
							$this->addError("Invalid Email");
						}
						break;
					case 'numeric': 
						if (!is_numeric($value)) {
							$this->addError("{$this->_label} is not a valid value.");
						}
						break;
					case 'alpha': 
						if (!preg_match('/^[a-z-A-ZñÑ ]*$/', $value)) {
							$this->addError("{$this->_label} should be alpha.");
						}
						break;
					case 'alphaNumChars':
						if (!preg_match('/^[\w\-\s ,()-]+$/', $value)) {
							$this->addError("{$this->_label} has invalid value.");
						}
						break;
					case 'truePassword':
						$id = Session::get('user_id');
						$check = $this->_db->query("SELECT * FROM users WHERE user_id = ?", array($id));

							$pass = $this->_db->onlyResult()->password;

							if (!password_verify($value, $pass)) {
								$this->addError("Doesn't match current password");
							}
						break;

					
					default:
						
						break;
				}
				
			}
		}

		if (empty($this->_errors)) {
			$this->_passed = true;
		}

		return $this;
	}

	public function addError($error) {
		$this->_errors[] = $error;
	}

	public function errors() {
		return $this->_errors;
	}

	public function passed() {
		return $this->_passed;
	}

	public function greet() {
		echo 'Hello';
	}

}