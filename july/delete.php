<?php 

require_once 'functions.php';

if (isset($_GET['delete'])) {
	$id =  $_GET['delete'];
	$query = $pdo->prepare("DELETE FROM users WHERE id = :id");
	$action = $query->execute([':id' => $id]);

	if ($action) {
		header("Location: http://localhost:8080/development/php_practice/july/index.php");
	}
}