<?php 

try {
	$pdo = new PDO("mysql:host=localhost;dbname=june", "root", "");	
} catch (PDOException $e) {
	echo $e->getMessage();
}

$sql = "SELECT * FROM users";
$result = $pdo->query($sql);

while ($row = $result->fetch(PDO::FETCH_OBJ)) {
	$users[] = $row;
}


if (isset($_POST['submit'])) {
	$name = $_POST['name'];
	$email = $_POST['email'];
	$age = $_POST['age'];

	$query = $pdo->prepare("INSERT INTO users (name, age, email) VALUES (?, ?, ?)");
	$action = $query->execute([$name, $age, $email]);

	if ($action) {
		echo "{$name} is created";
	}
}
