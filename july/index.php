<?php require_once 'functions.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<article>
		<h1>List of users</h1>
		<ul>
			<?php foreach($users as $user): ?>
			<li><?php echo $user->name; ?> - 
				<a href="http://localhost:8080/development/php_practice/july/update.php?id=<?php echo $user->id; ?>">
				UPDATE
				</a>
				-
				<button type="submit" data-js="delete" value="<?php echo $user->id; ?>">DELETE</button>
			</li>
			<?php endforeach; ?>
		</ul>
	</article>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
		<label>
			Name:
			<input type="text" name="name">
		</label>
		<label>
			Email
			<input type="email" name="email">	
		</label>
		<label>
			Age
			<input type="number" name="age">	
		</label>
		<input type="submit" name="submit">	
	</form>

	<script>
		let deleteButtons = document.querySelectorAll("[data-js=delete]")

		deleteButtons.forEach((deleteButton) => {
			deleteButton.addEventListener('click', (e) => {
				let result = confirm("Are you sure you want to delete?");
				if (result) {
					// alert(e.target.value)
					document.location = `http://localhost:8080/development/php_practice/july/delete.php?delete=${e.target.value}`
				}
			})
		})
	</script>
</body>
</html>