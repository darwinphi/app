<?php 

try {
	$pdo = new PDO("mysql:host=localhost;dbname=june", "root", "");	
} catch (PDOException $e) {
	echo $e->getMessage();
}

$sql = "SELECT * FROM users";
$result = $pdo->query($sql);

while ($row = $result->fetch(PDO::FETCH_OBJ)) {
	$users[] = $row;
}

foreach ($users as $user) {
	echo $user->name . "<br>";
}

// INSERT
$email = "john@email.com";
$name = "John Doe";
$age = 25;

$query = $pdo->prepare("INSERT INTO users (email, name, age) VALUES (?, ?, ?)");
// $action = $query->execute([$email, $name, $age]);

// if ($action) {
// 	echo 'User created';
// }

// UPDATE
$email = "john@email.com";
$name = "John Doe Jr.";
$age = 25;

$query = $pdo->prepare("UPDATE users SET name = ?, age = ? WHERE email = ?");
// $action = $query->execute([$name, $age, $email]);

// if ($action) {
// 	echo 'User Updated';
// }

// DELETE
$email = "john@email.com";
$query = $pdo->prepare("DELETE FROM users WHERE email = ?");
// $action = $query->execute([$email]);

// if ($action) {
// 	echo "User deleted";
// }

// SELECT BY ID
$id = 1;
$query = $pdo->prepare("SELECT * FROM users WHERE id = ?");
$result = $query->execute([$id]);

// $user = $query->fetch(PDO::FETCH_OBJ);
// echo $user->name;

while ($row = $query->fetch(PDO::FETCH_OBJ)) {
	echo $row->email;
}
