<?php
	require_once 'functions.php';

	if (isset($_GET['id'])) {
		$id = $_GET['id'];
		
		$query = $pdo->prepare("SELECT * FROM users WHERE id = ?");
		$query->execute([$id]);
		$user = $query->fetch(PDO::FETCH_OBJ);
	}

	if (isset($_POST['update'])) {
		$id = $_POST['id'];
		$name = $_POST['name'];
		$age = $_POST['age'];

		$query = $pdo->prepare("UPDATE users SET name = ?, age = ? WHERE id = ?");
		$action = $query->execute([$name, $age, $id]);

		if ($action) {
			echo "User updated";
		}
	}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Update</title>
</head>
<body>
	<a href="http://localhost:8080/development/php_practice/july/index.php">Back to Home</a>
	<h1>UPDATE USER</h1>
	<form action="http://localhost:8080/development/php_practice/july/update.php?id=<?php echo $user->id; ?>" method="POST">
		<label for="">
			Name:
			<input type="text" name="name" value="<?php echo $user->name; ?>">
		</label>
		<label for="">
			Age:
			<input type="number" name="age" value="<?php echo $user->age; ?>">
		</label>
		<input type="hidden" name="id" value="<?php echo $user->id; ?>">
		<input type="submit" name="update">
	</form>
</body>
</html>