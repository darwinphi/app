<?php

try {
	$pdo = new PDO("mysql:host=localhost;dbname=june", "root", "");
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	echo "Connected <br>";
} catch (PDOException $e) {
	echo $e->getMessage();
	die();
}

// GET
$sql = "SELECT * FROM users";
$result = $pdo->query($sql);

while ($row = $result->fetch(PDO::FETCH_OBJ)) {
	$users[] = $row;
}

foreach ($users as $user) {
	echo "Name: {$user->name}; Age: {$user->age} <br>";
}

// SELECT USER
$id = 1;
$query = $pdo->prepare("SELECT * FROM users WHERE id = ?");
$result = $query->execute([$id]);

$selectedUser = $query->fetch(PDO::FETCH_OBJ);
echo "Selected User: {$selectedUser->name} <br>";

function exists($email, $pdo) {
	$query = $pdo->prepare("SELECT COUNT(*) FROM users WHERE email = ?");
	$result = $query->execute([$email]);
	$selectedEmail = $query->fetch();
	if ($selectedEmail[0] > 0) {
		return true;
	}
}

// INSERT
// $email = 'john@email.com';
// $name = 'John';
// $age = 22;

// if (exists($email, $pdo)) {
// 	echo "Email already exists <br>";
// }
// else {
// 	$query = $pdo->prepare("INSERT INTO users (email, name, age) VALUES (?, ?, ?)");
// 	$action = $query->execute([$email, $name, $age]);

// 	if ($action) {
// 		echo "{$email} inserted";
// 	}
// }

// UPDATE
$name = 'Darwina';
$age = 22;
$email = 'darwin@email.com';

$query = $pdo->prepare("UPDATE users SET name = ?, age = ? WHERE email = ?");
$action = $query->execute([$name, $age, $email]);
if ($action) {
	echo 'User updated';
}

// DELETE
$email = 'john@email.com';
$query = $pdo->prepare("DELETE FROM users WHERE email = ?");
$action = $query->execute([$email]);

if ($action) {
	echo "User deleted";
}
