<?php 

try {
	$pdo = new PDO("mysql:host=localhost;dbname=ajax_project", "root", "");
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	echo "Connected";
} catch (PDOException $e) {
	echo $e->getMessage();
	die();
}

// SELECT ALL
$sql = "SELECT * FROM users";
$result = $pdo->query($sql);

while ($row = $result->fetch(PDO::FETCH_OBJ)) {
	$users[] = $row;
}

// INSERT 
if (isset($_POST['submit'])) {
	$name = htmlentities($_POST['name'], ENT_QUOTES, "UTF-8");
	$age = htmlentities($_POST['age'], ENT_QUOTES, "UTF-8");

	$query = $pdo->prepare("INSERT INTO users (name, age) VALUES (:name, :age)");
	$action = $query->execute([':name' => $name, ':age' => $age]);
	
	if ($action) {
		header("Location: table.php");
	}
}

// SELECT ID

if (isset($_GET['update'])) {
	$id = $_GET['id'];
	$query = $pdo->prepare("SELECT * FROM users WHERE id = :id");
	$result = $query->execute([':id' => $id]);


	while ($row = $query->fetch(PDO::FETCH_OBJ)) {
		$userID[] = $row;
	}

	foreach ($userID as $user) {
		echo $user->name;
	}
}

// UPDATE 

if (isset($_POST['update'])) {
	$name =  $_POST['name'];
	$age = $_POST['age'];
	$id = $_POST['id'];

	$query = $pdo->prepare("UPDATE users SET name = :name, age = :age WHERE id = :id");
	$action = $query->execute([':name' => $name, ':age' => $age, ':id' => $id]);

	if ($action) {
		echo 'UPDATED';
		header('Location: table.php');
	}
}

// DELETE
if (isset($_POST['delete'])) {
	$id =  $_POST['id'];
	$query = $pdo->prepare("DELETE FROM users WHERE id = :id");
	$action = $query->execute([':id' => $id]);

	if ($action) {
		header("Location: table.php");
	}
}

// $id = 1;
// $query = $pdo->prepare("SELECT * FROM users WHERE id = :value");
// $result = $query->execute(['value' => $id]);
// while ($row = $query->fetch(PDO::FETCH_OBJ)) {
// 	$userss[] = $row;
// }

// foreach ($userss as $user) {
// 	echo $user->name;
// }

// REGISTER
if (isset($_POST['register'])) {
	$name = $_POST['name'];
	$age = $_POST['age'];
	$password = $_POST['password'];
	$hash_password = password_hash($password, PASSWORD_DEFAULT);
	
	$query = $pdo->prepare("INSERT INTO users (name, age, password) VALUES (?, ?, ?)");
	$action = $query->execute([$name, $age, $hash_password]);

	if ($action) {
		echo "${name} IS REGISTERED";
	}
}

// LOGIN
if (isset($_POST['login'])) {
	$name = $_POST['name'];
	$password = $_POST['password'];

	$query = $pdo->prepare("SELECT * FROM users WHERE name = ?");
	$result = $query->execute([$name]);
	$user = $query->fetch(PDO::FETCH_OBJ);
	
	if (password_verify($password, $user->password)) {
		$_SESSION['name'] = $user->name;
	}
	else {
		echo 'Either name or password is incorrect';
	}
}

// LOGOUT
if (isset($_POST['logout'])) {
	session_destroy();
	header("Location: table.php");
}




