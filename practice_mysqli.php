<?php 

$db = new mysqli('localhost', 'root', '', 'ajax_project');

// SELECT ALL
$result = $db->query("SELECT * FROM users");

while ($row = $result->fetch_object()) {
	$users[] = $row;
}

foreach ($users as $user) {
	echo $user->name . '<br>';
}

// NO. OF ALL USERS

echo 'Total Users:' . $result->num_rows;

// SELECT BY NAME

$stmt = $db->prepare("SELECT * FROM users WHERE name = ?");
$name = 'John';
$stmt->bind_param('s', $name);
$stmt->execute();
$result = $stmt->get_result();

// var_dump($result);

while ($row = $result->fetch_object()) {
	$users[] = $row;
}

foreach ($users as $user) {
	// echo $user->name;
}

// DELETE

$stmt = $db->prepare("DELETE FROM users WHERE id = ?");
$id = 16;
$stmt->bind_param('d', $id);
// $stmt->execute();
echo 'No. of deleted users:' . $db->affected_rows . '<br>';
$stmt->close();

// INSERT 
$stmt = $db->prepare("INSERT INTO users (name, age) VALUES (?, ?)");
$name = 'John';
$age = 18;
$stmt->bind_param('sd', $name, $age);
// $stmt->execute();

echo 'No. of added users: ' . $db->affected_rows . '<br>';
$stmt->close();

// UPDATE 
$stmt = $db->prepare("UPDATE users SET name = ?, age = ? WHERE id = ?");
$name = 'Josh';
$age = 69;
$id = 18;
$stmt->bind_param('sdd', $name, $age, $id);
$stmt->execute();

echo 'No. of updated users: ' . $db->affected_rows . '<br>';
$stmt->close();


$pdo = new PDO("mysql:host=localhost;dbname=ajax_project", "root", "");

// COMPARING PDO & MYSQLI

// PDO - SELECT
$result = $pdo->query("SELECT * FROM users");
while ($row = $result->fetch(PDO::FETCH_OBJ)) {
	echo $row->name;
}

// MYSQLI - SELECT
$result = $db->query("SELECT * FROM users");
while ($row = $result->fetch_object()) {
	echo $row->name;
}

// PDO - SELECT BY ID
$query = $pdo->prepare("SELECT * FROM user WHERE id = :id");
$result = $query->execute([':id' => 18]);

while ($row = $query->fetch(PDO::FETCH_OBJ)) {
	echo $row->name;
}

// MYSQLI - SELECY BY ID

$stmt = $db->prepare("SELECT * FROM users WHERE id = ?");
$id = 18;
$stmt->bind_param('d', $id);
$stmt->execute();
$result = $stmt->get_result();

while ($row = $result->fetch_object()) {
	echo $row->name;
}

// PDO - INSERT
$query = $pdo->prepare("INSERT INTO users (name, age) VALUES (?, ?)");
// $action = $query->execute(['Darwin', 29]);

// if ($action) {
// 	echo 'ADDED';
// }

// MYSQLI - INSERT
$name = "Darwin";
$age = 23;

$stmt = $db->prepare("SELECT * FROM users WHERE name = ?");
$stmt->bind_param('s', $name);
$stmt->execute();

echo $db->affected_rows;

// $stmt = $db->prepare("INSERT INTO users (name, age) VALUES (?, ?)");

// $stmt->bind_param('sd', $name, $age);
// $action = $stmt->execute();

// if ($action) {
// 	echo 'ADDED';
// }

