<?php require_once 'practice.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>UPDATE</title>
</head>
<body>
	<form action="table.php" method="POST">
		<?php foreach ($userID as $user): ?>
			<input type="text" value="<?php echo $user->name; ?>" name="name" placeholder="Name">
			<input type="text" value="<?php echo $user->age; ?>" name="age" placeholder="Age">
			<input type="hidden" value="<?php echo $user->id; ?>" name="id">
		<?php endforeach; ?>			
		<button type="submit" name="update">Update</button>
	</form>
</body>
</html>