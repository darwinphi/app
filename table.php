<?php 
	ob_start();
	session_start();
	
	require_once 'practice.php';
	
	if (isset($_SESSION['name'])) {
		echo "You are login, {$_SESSION['name']}";
	}
	else {
		echo "You are not login";
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<style>
	body {
		font-family: 'Segoe UI', sans-serif;
	}
	table {
		border-collapse: collapse;
	}

	table, th, td {
		padding: 1em;
		border: 1px solid black;
	}
</style>
<body>
	
	<h2>Register</h2>
	<form action="table.php" method="POST">
		<p>
			<label for="">Name</label>
			<input type="text" name="name">
		</p>
		<p>
			<label for="">Age</label>
			<input type="number" name="age">
		</p>
		<p>
			<label for="">Password</label>
			<input type="password" name="password">
		</p>
		<button type="submit" name="register">Register</button>
	</form>

	<hr><br><br>
	
	<?php if (!isset($_SESSION['name'])): ?>
		<h2>Log In</h2>
		<form action="table.php" method="POST">
			<p>
				<label for="">Name</label>
				<input type="text" name="name">
			</p>
			<p>
				<label for="">Password</label>
				<input type="password" name="password">
			</p>
			<button type="submit" name="login">Login</button>
		</form>
	<?php else: ?>
		<form action="table.php" method="POST">
			<button type="submit" name="logout">Logout</button>
		</form>
	<?php endif; ?>
	

	<hr><br><br>

	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
		<input type="text" name="name" placeholder="Name">
		<input type="number" name="age" placeholder="Age">
		<button type="submit" name="submit">Submit</button>
	</form>
	<table>
		<thead>
			<tr>
				<th>Names</th>
				<th>Age</th>
				<th colspan="2">ACTION</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($users as $user): ?>
			<tr>
				<td><?php echo $user->name; ?></td>
				<td><?php echo $user->age; ?></td>
				<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
					<input type="hidden" name="id" value="<?php echo $user->id; ?>">
					<td><button type="submit" name="delete">DELETE</button></td>
				</form>
				<form action="table-update.php" method="GET">
					<input type="hidden" name="id" id="js-id" value="<?php echo $user->id; ?>">
					<td>
						<button type="submit" name="update" class="js-item" value="<?php echo $user->id; ?>">UPDATE</button>
					</td>
				</form>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<ul id="js-lists">
		
	</ul>

	<script>

		// 1st Solution

		// var lists = document.getElementById('js-lists');
		// lists.addEventListener('click', function(e) {
			
		// 	var result = window.confirm("Are you sure you want to update?");
		// 	if (result) {
		// 		document.location = 'table-update.php?id=' + e.target.value + '&update=' + e.target.value;
		// 		console.log(e.target.value);
		// 	}
		// })
		
		// 2nd Solution

		var items = document.querySelectorAll('.js-item');

		items.forEach(function(item) {
			item.addEventListener('click', function(e) {
				var result = window.confirm("Are you sure you want to update?");
				if (result) {
					document.location = 'table-update.php?id=' + e.target.value + '&update=' + e.target.value;
					console.log(e.target.value);
				}
			})
		})	
	</script>
</body>
</html>